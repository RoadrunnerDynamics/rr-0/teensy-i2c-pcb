# Setup Library 

- In `Preferences > Manage Symbol Library` add the `RR0_parts_lib.kicad_sym` from `parts_lib` of this repo
- In `Preferences` add the `RR0_parts_lib.pretty` from the `parts_lib` of this repoo

# Errata 

## Rev 0.0
- Silkscreen missing various labels. Include in next design (R values, Wire number)
- Maybe use a Resistor network next time
- I recommend not not routing main SCL and SDA bus through the THT connectors next time. Instead shorter pick off lines to those holes.
